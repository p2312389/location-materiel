import { Component } from '@angular/core';
import { RouterOutlet } from '@angular/router';
import { ObjetsComponent } from "./components/objets/objets.component";
import { ToolbarComponent } from "./components/toolbar/toolbar.component";

@Component({
  selector: 'app-root',
  standalone: true,
  imports: [RouterOutlet, ObjetsComponent, ToolbarComponent],
  templateUrl: './app.component.html',
  styleUrl: './app.component.scss'
})
export class AppComponent {
  title = 'location-materiel';
}
