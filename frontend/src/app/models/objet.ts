import { Transaction } from "./transaction";
import { Utilisateur } from "./utilisateur";

export interface IObjet {
  id: number;
  nom: string;
  prix: number;
  duree: number;
  transactions?: Transaction[];
  proprietaire: Utilisateur;
}

export class Objet implements IObjet {
  id: number;
  nom: string;
  prix: number;
  duree: number;
  transactions?: Transaction[];
  proprietaire: Utilisateur;

  constructor(objet: IObjet) {
    this.id = objet.id;
    this.nom = objet.nom;
    this.prix = objet.prix;
    this.duree = objet.duree;
    this.transactions = objet.transactions;
    this.proprietaire = objet.proprietaire;
  }
}

export class ObjetDto {
  id?: number;
  nom: string;
  prix: number;
  duree: number;
  idProprietaire: number;

  constructor(objet: Objet) {
    this.id = objet.id;
    this.nom = objet.nom;
    this.prix = objet.prix;
    this.duree = objet.duree;
    this.idProprietaire = objet.proprietaire.id;
  }
}
