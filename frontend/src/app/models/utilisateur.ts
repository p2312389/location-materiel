import { Objet } from "./objet";

export interface IUtilisateur {
  id: number;
  prenom: string;
  nom: string;
  evaluation: number;
  adresse: string;
  ville: string;
  codePostal: string;
  objets: Objet[];
}

export class Utilisateur implements IUtilisateur {
  id: number;
  prenom: string;
  nom: string;
  evaluation: number;
  adresse: string;
  ville: string;
  codePostal: string;
  objets: Objet[];

  constructor(utilisateur: IUtilisateur) {
    this.id = utilisateur.id;
    this.prenom = utilisateur.prenom;
    this.nom = utilisateur.nom;
    this.evaluation = utilisateur.evaluation;
    this.adresse = utilisateur.adresse;
    this.ville = utilisateur.ville;
    this.codePostal = utilisateur.codePostal;
    this.objets = utilisateur.objets;
  }
}

export class UtilisateurDto {
  id?: number;
  prenom: string;
  nom: string;
  evaluation: number;
  adresse: string;
  ville: string;
  codePostal: string;

  constructor(utilisateur: Utilisateur) {
    this.id = utilisateur.id;
    this.prenom = utilisateur.prenom;
    this.nom = utilisateur.nom;
    this.evaluation = utilisateur.evaluation;
    this.adresse = utilisateur.adresse;
    this.ville = utilisateur.ville;
    this.codePostal = utilisateur.codePostal;
  }
}
