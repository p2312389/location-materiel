import { Utilisateur } from "./utilisateur";
import { Objet } from "./objet";

export interface ITransaction {
  id: number;
  dateDebut: Date;
  dateFin: Date;
  temps: number;
  loueur: Utilisateur;
  client: Utilisateur;
  objet: Objet;
}

export class Transaction implements ITransaction {
  id: number;
  dateDebut: Date;
  dateFin: Date;
  temps: number;
  loueur: Utilisateur;
  client: Utilisateur;
  objet: Objet;

  constructor(transactions: Transaction) {
    this.id = transactions.id;
    this.dateDebut = transactions.dateDebut;
    this.dateFin = transactions.dateFin;
    this.temps = transactions.temps;
    this.loueur = transactions.loueur;
    this.client = transactions.client;
    this.objet = transactions.objet;
  }
}

export class TransactionDto {
  id?: number;
  dateDebut: Date;
  dateFin: Date;
  temps?: number;
  idLoueur?: number;
  idClient?: number;
  idObjet?: number;

  constructor(transactions: Transaction, idLoueur?: number, idClient?: number, idObjet?: number) {
    this.dateDebut = transactions.dateDebut;
    this.dateFin = transactions.dateFin;
    this.temps = transactions.temps;
    this.idLoueur = idLoueur;
    this.idClient = idClient;
    this.idObjet = idObjet;
  }
}
