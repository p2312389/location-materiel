import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { map, Observable } from "rxjs";
import { IObjet, Objet, ObjetDto } from '../models/objet';
import { IUtilisateur, Utilisateur, UtilisateurDto } from '../models/utilisateur';
import { ITransaction, Transaction, TransactionDto } from "../models/transaction";

@Injectable({
  providedIn: 'root'
})
export class ApiService {

  constructor(private http: HttpClient) {
  }

  private readonly _apiUrl: string = 'http://localhost:8000/api';

  getObjets(): Observable<Objet[]> {
    return this.http.get<any>(`${this._apiUrl}/objets`).pipe(
      map((response) => {
        return response['hydra:member'].map((dto: IObjet) => new Objet(dto))
      })
    );
  }

  creerObjet(objet: ObjetDto) {
    return this.http.post(`${this._apiUrl}/objet`, objet);
  }

  modifierObjet(objet: Objet) {
    this.http.put(`${this._apiUrl}/objet/${objet.id}`, new ObjetDto(objet)).subscribe(response => {
      console.log(response);
    });
  }

  supprimerObjet(id: number) {
    return this.http.delete<any>(`${this._apiUrl}/objets/${id}`);
  }

  getUtilisateurs(): Observable<Utilisateur[]> {
    return this.http.get<any>(`${this._apiUrl}/utilisateurs`).pipe(
      map((response) => {
        return response['hydra:member'].map((dto: IUtilisateur) => new Utilisateur(dto))
      })
    );
  }

  creerUtilisateur(utilisateur: UtilisateurDto) {
    return this.http.post<Utilisateur>(`${this._apiUrl}/utilisateur`, utilisateur);
  }

  modifierUtilisateur(utilisateur: Utilisateur) {
    this.http.put(`${this._apiUrl}/utilisateur/${utilisateur.id}`, new UtilisateurDto(utilisateur)).subscribe(response => {
      console.log(response);
    });
  }

  supprimerUtilisateur(id: number) {
    return this.http.delete<any>(`${this._apiUrl}/utilisateurs/${id}`);
  }

  getTransactions(): Observable<Transaction[]> {
    return this.http.get<any>(`${this._apiUrl}/transactions`).pipe(
      map((response) => {
        return response['hydra:member'].map((dto: ITransaction) => new Transaction(dto))
      })
    );
  }

  creerTransaction(transaction: TransactionDto) {
    return this.http.post<Utilisateur>(`${this._apiUrl}/transaction`, transaction);
  }

  modifierTransaction(transaction: Transaction) {
    this.http.put(`${this._apiUrl}/transaction/${transaction.id}`, new TransactionDto(transaction)).subscribe(response => {
      console.log(response);
    });
  }

  supprimerTransaction(id: number) {
    return this.http.delete<any>(`${this._apiUrl}/transactions/${id}`);
  }
}
