import { Routes } from '@angular/router';
import { ObjetsComponent } from "./components/objets/objets.component";
import { TransactionsComponent } from "./components/transactions/transactions.component";
import { UtilisateursComponent } from "./components/utilisateurs/utilisateurs.component";
import { HomeComponent } from "./components/home/home.component";

export const routes: Routes = [
  { path: '', component: HomeComponent },
  { path: 'transactions',  component: TransactionsComponent },
  { path: 'objets',  component: ObjetsComponent },
  { path: 'utilisateurs',  component: UtilisateursComponent },
  { path: '**', redirectTo: '' }
];
