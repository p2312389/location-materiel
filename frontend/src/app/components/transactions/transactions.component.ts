import { Component, OnInit } from '@angular/core';
import { ApiService } from "../../services/api.service";
import { MatDialog } from "@angular/material/dialog";
import { Transaction } from "../../models/transaction";
import {
  CreerModifierTransactionModalComponent
} from "./creer-modifier-transaction-modal/creer-modifier-transaction-modal.component";
import { ListeUtilisateurComponent } from "../utilisateurs/liste-utilisateur/liste-utilisateur.component";
import { MatButton } from "@angular/material/button";
import { ListeTransactionComponent } from "./liste-transaction/liste-transaction.component";

@Component({
  selector: 'app-transactions',
  standalone: true,
  imports: [
    ListeUtilisateurComponent,
    MatButton,
    ListeTransactionComponent
  ],
  templateUrl: './transactions.component.html',
  styleUrl: './transactions.component.scss'
})
export class TransactionsComponent implements OnInit {
  transactions: Transaction[] = [];

  constructor(private api: ApiService, public dialog: MatDialog) {
  }

  ngOnInit() {
    this.getTransactions()
  }

  creerTransaction() {
    const dialogRef = this.dialog.open(CreerModifierTransactionModalComponent, {
      height: '400px',
      width: '800px',
      data: {action: 'creer', transaction: {}}
    });

    dialogRef.afterClosed().subscribe(transaction => {
      if (transaction) {
        this.api.creerTransaction(transaction).subscribe({
          next: () => {
            this.getTransactions
          }
        });
      }
    });
  }

  getTransactions() {
    this.api.getTransactions().subscribe((transactions) => {
      this.transactions = transactions;
    });
  }
}
