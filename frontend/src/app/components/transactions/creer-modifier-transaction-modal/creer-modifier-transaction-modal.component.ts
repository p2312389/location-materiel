import { Component, Inject, OnInit } from '@angular/core';
import { FormControl, FormGroup, ReactiveFormsModule, Validators } from "@angular/forms";
import { MAT_DIALOG_DATA, MatDialogRef } from "@angular/material/dialog";
import { Utilisateur } from "../../../models/utilisateur";
import { Transaction, TransactionDto } from "../../../models/transaction";
import { MatFormField, MatFormFieldModule } from "@angular/material/form-field";
import {
  MatDatepicker,
  MatDatepickerInput,
  MatDatepickerModule,
  MatDatepickerToggle
} from "@angular/material/datepicker";
import { Objet } from "../../../models/objet";
import { ApiService } from "../../../services/api.service";
import { MatInput, MatInputModule } from "@angular/material/input";
import { MatOption, MatSelect } from "@angular/material/select";
import { MatButton } from "@angular/material/button";
import { MAT_DATE_LOCALE, MatNativeDateModule, provideNativeDateAdapter } from "@angular/material/core";

interface dataModal {
  action: 'creer' | 'modifier';
  transaction?: Transaction;
}

@Component({
  selector: 'app-creer-modifier-transaction-modal',
  standalone: true,
  providers: [
    {provide: MAT_DATE_LOCALE, useValue: 'fr-FR'},
    provideNativeDateAdapter()
  ],
  imports: [
    ReactiveFormsModule,
    MatFormField,
    MatDatepickerInput,
    MatDatepickerToggle,
    MatInput,
    MatDatepicker,
    MatSelect,
    MatFormFieldModule,
    MatInputModule,
    MatDatepickerModule,
    MatOption,
    MatButton,
    MatNativeDateModule
  ],
  templateUrl: './creer-modifier-transaction-modal.component.html',
  styleUrl: './creer-modifier-transaction-modal.component.scss'
})
export class CreerModifierTransactionModalComponent implements OnInit {
  utilisateurs: Utilisateur[] = [];
  objets: Objet[] = []
  clientSelectionne: Utilisateur | undefined = this.data.transaction?.loueur;
  loueurSelectionne: Utilisateur | undefined = this.data.transaction?.client;
  objetSelectionne: Objet | undefined = this.data.transaction?.objet;

  transactionFormGroup: FormGroup = new FormGroup({
    dateDebut: new FormControl(this.data.transaction?.dateDebut, Validators.required),
    dateFin: new FormControl(this.data.transaction?.dateFin, Validators.required),
    loueur: new FormControl((this.data.transaction?.loueur || undefined), Validators.required),
    client: new FormControl((this.data.transaction?.client || undefined), Validators.required),
    objet: new FormControl((this.data.transaction?.objet || undefined), Validators.required)
  });

  constructor(
    public dialogRef: MatDialogRef<CreerModifierTransactionModalComponent>,
    @Inject(MAT_DIALOG_DATA) public data: dataModal, private api: ApiService) {
  }

  ngOnInit() {
    console.log(this.loueurSelectionne)
    this.api.getUtilisateurs().subscribe(utilisateurs => {
      this.utilisateurs = utilisateurs
      if (this.data.transaction?.loueur) {
        this.loueurSelectionne = this.utilisateurs.find(utilisateur => utilisateur.id == this.data.transaction?.loueur.id);
        this.clientSelectionne = this.utilisateurs.find(utilisateur => utilisateur.id == this.data.transaction?.client.id);
      }
    });
    this.api.getObjets().subscribe(objets => {
      this.objets = objets
      if (this.data.transaction?.objet) {
        this.objetSelectionne = this.objets.find(objet => objet.id == this.data.transaction?.objet.id);
      }
    });
  }

  onNoClick(): void {
    this.dialogRef.close();
  }

  creerTransaction() {
    let nouvelleTransaction: TransactionDto = {
      dateDebut: this.transactionFormGroup.controls['dateDebut'].value,
      dateFin: this.transactionFormGroup.controls['dateFin'].value,
      idLoueur: this.transactionFormGroup.controls['loueur'].value.id,
      idClient: this.transactionFormGroup.controls['client'].value.id,
      idObjet: this.transactionFormGroup.controls['objet'].value.id
    }
    this.dialogRef.close(nouvelleTransaction);
  }

  modifierTransaction() {
    if (this.data.transaction) {
      this.data.transaction.dateDebut = this.transactionFormGroup.controls['dateDebut'].value;
      this.data.transaction.dateFin = this.transactionFormGroup.controls['dateFin'].value;
      this.data.transaction.loueur = this.transactionFormGroup.controls['loueur'].value;
      this.data.transaction.client = this.transactionFormGroup.controls['client'].value;
      this.data.transaction.objet = this.transactionFormGroup.controls['objet'].value;

      this.dialogRef.close(this.data.transaction);
    }
  }
}
