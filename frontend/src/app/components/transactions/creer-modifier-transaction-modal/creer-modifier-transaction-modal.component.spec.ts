import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CreerModifierTransactionModalComponent } from './creer-modifier-transaction-modal.component';

describe('CreerModifierTransactionModalComponent', () => {
  let component: CreerModifierTransactionModalComponent;
  let fixture: ComponentFixture<CreerModifierTransactionModalComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [CreerModifierTransactionModalComponent]
    })
    .compileComponents();
    
    fixture = TestBed.createComponent(CreerModifierTransactionModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
