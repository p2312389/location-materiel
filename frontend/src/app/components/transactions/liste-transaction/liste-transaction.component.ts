import { Component, Input } from '@angular/core';
import { ApiService } from "../../../services/api.service";
import { MatDialog } from "@angular/material/dialog";
import { MatTableModule } from "@angular/material/table";
import { Transaction } from "../../../models/transaction";
import {
  CreerModifierTransactionModalComponent
} from "../creer-modifier-transaction-modal/creer-modifier-transaction-modal.component";
import { MatButton } from "@angular/material/button";
import { DatePipe } from "@angular/common";

@Component({
  selector: 'app-liste-transaction',
  standalone: true,
  imports: [
    MatTableModule,
    MatButton,
    DatePipe
  ],
  templateUrl: './liste-transaction.component.html',
  styleUrl: './liste-transaction.component.scss'
})
export class ListeTransactionComponent {
  @Input() transactions: Transaction[] = [];
  displayedColumns: string[] = ['dateDebut', 'dateFin', 'loueur', 'client', 'objet', 'actions'];

  constructor(private api: ApiService, public dialog: MatDialog) {
  }

  modifierTransaction(transaction: Transaction) {
    const dialogRef = this.dialog.open(CreerModifierTransactionModalComponent, {
      height: '400px',
      width: '800px',
      data: {action: 'modifier', transaction: transaction}
    });

    dialogRef.afterClosed().subscribe(transaction => {
      if (transaction) {
        this.api.modifierTransaction(transaction);
      }
    });
  }

  supprimerTransaction(id: number) {
    this.api.supprimerTransaction(id).subscribe({
      next: () => {
        this.transactions = this.transactions.filter(transaction => transaction.id !== id);
      }
    });
  }
}
