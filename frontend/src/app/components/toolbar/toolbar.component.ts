import { Component } from '@angular/core';
import { MatToolbar } from "@angular/material/toolbar";
import { MatButton, MatIconButton } from "@angular/material/button";
import { Router } from "@angular/router";

@Component({
  selector: 'app-toolbar',
  standalone: true,
  imports: [
    MatToolbar,
    MatIconButton,
    MatButton
  ],
  templateUrl: './toolbar.component.html',
  styleUrl: './toolbar.component.scss'
})
export class ToolbarComponent {
  constructor(private _router: Router) {
  }

  naviguerVersAccueil() {
    this._router.navigateByUrl('');
  }

  naviguerVersObjets() {
    this._router.navigateByUrl('objets')
  }

  naviguerVersUtilisateurs() {
    this._router.navigateByUrl('utilisateurs')
  }

  naviguerVersTransactions() {
    this._router.navigateByUrl('transactions')
  }
}
