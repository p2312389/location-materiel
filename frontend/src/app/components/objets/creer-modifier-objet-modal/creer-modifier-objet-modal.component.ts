import { Component, Inject, OnInit } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogClose, MatDialogRef } from '@angular/material/dialog';
import { IObjet, Objet, ObjetDto } from '../../../models/objet';
import { MatButton } from '@angular/material/button';
import { MatFormField, MatLabel } from '@angular/material/form-field';
import { FormControl, FormGroup, ReactiveFormsModule, Validators } from '@angular/forms';
import { MatOption, MatSelect } from '@angular/material/select';
import { ApiService } from '../../../services/api.service';
import { Utilisateur } from '../../../models/utilisateur';
import { MatInput } from '@angular/material/input';

interface dataModal {
  action: 'creer' | 'modifier';
  objet?: Objet;
}

@Component({
  selector: 'app-creer-modifier-objet-modal',
  standalone: true,
  imports: [
    MatDialogClose,
    MatButton,
    MatFormField,
    ReactiveFormsModule,
    MatSelect,
    MatOption,
    MatInput,
    MatLabel
  ],
  templateUrl: './creer-modifier-objet-modal.component.html',
  styleUrl: './creer-modifier-objet-modal.component.scss'
})
export class CreerModifierObjetModalComponent implements OnInit {
  utilisateurs: Utilisateur[] = [];

  objetFormGroup: FormGroup = new FormGroup({
    nom: new FormControl((this.data.objet?.nom || ''), Validators.required),
    prix: new FormControl((this.data.objet?.prix || 0), Validators.required),
    duree: new FormControl((this.data.objet?.duree || 0), Validators.required),
    utilisateur: new FormControl((this.data.objet?.proprietaire || undefined), Validators.required)
  })

  constructor(
    public dialogRef: MatDialogRef<CreerModifierObjetModalComponent>,
    @Inject(MAT_DIALOG_DATA) public data: dataModal,
    private apiService: ApiService) {
  }

  ngOnInit() {
    this.apiService.getUtilisateurs().subscribe(utilisateurs => this.utilisateurs = utilisateurs);
  }

  onNoClick(): void {
    this.dialogRef.close();
  }

  creerObjet() {
    let proprietaire = this.objetFormGroup.controls['utilisateur'].value
    let nouveauObjet: ObjetDto = {
      nom: this.objetFormGroup.controls['nom'].value,
      prix: parseInt(this.objetFormGroup.controls['prix'].value),
      duree: parseInt(this.objetFormGroup.controls['duree'].value),
      idProprietaire: proprietaire.id,
    }

    this.dialogRef.close(nouveauObjet);
  }

  modifierObjet() {
    if (this.data.objet) {
      this.data.objet.nom = this.objetFormGroup.controls['nom'].value;
      this.data.objet.prix = parseInt(this.objetFormGroup.controls['prix'].value);
      this.data.objet.duree = parseInt(this.objetFormGroup.controls['duree'].value);
      this.data.objet.proprietaire = this.objetFormGroup.controls['utilisateur'].value;

      this.dialogRef.close(this.data.objet);
    }
  }
}
