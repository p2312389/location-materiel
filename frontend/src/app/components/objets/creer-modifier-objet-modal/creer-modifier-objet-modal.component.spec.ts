import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CreerModifierObjetModalComponent } from './creer-modifier-objet-modal.component';

describe('CreerModifierObjetModalComponent', () => {
  let component: CreerModifierObjetModalComponent;
  let fixture: ComponentFixture<CreerModifierObjetModalComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [CreerModifierObjetModalComponent]
    })
    .compileComponents();
    
    fixture = TestBed.createComponent(CreerModifierObjetModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
