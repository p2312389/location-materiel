import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ListeObjetComponent } from './liste-objet.component';

describe('ListeObjetComponent', () => {
  let component: ListeObjetComponent;
  let fixture: ComponentFixture<ListeObjetComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [ListeObjetComponent]
    })
    .compileComponents();
    
    fixture = TestBed.createComponent(ListeObjetComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
