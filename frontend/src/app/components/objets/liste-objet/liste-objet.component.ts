import { Component, EventEmitter, Input, Output } from '@angular/core';
import { MatTableModule } from "@angular/material/table";
import { Objet } from "../../../models/objet";
import { ApiService } from "../../../services/api.service";
import { MatDialog } from "@angular/material/dialog";
import { CreerModifierObjetModalComponent } from "../creer-modifier-objet-modal/creer-modifier-objet-modal.component";
import { MatButton } from "@angular/material/button";
import { CardObjetComponent } from "../card-objet/card-objet.component";

@Component({
  selector: 'app-liste-objet',
  standalone: true,
  imports: [MatTableModule, MatButton, CardObjetComponent],
  templateUrl: './liste-objet.component.html',
  styleUrl: './liste-objet.component.scss'
})
export class ListeObjetComponent {
  @Input() objets: Objet[] = [];
  displayedColumns: string[] = ['nom', 'prix', 'duree', 'proprietaire', 'actions'];

  constructor(private api: ApiService, public dialog: MatDialog) {
  }

  modifierObjet(objet: Objet) {
    this.api.modifierObjet(objet);
  }

  supprimerObjet(id: number) {
    this.api.supprimerObjet(id).subscribe( {
      next: () => {
        this.objets = this.objets.filter(objet => objet.id !== id);
      }
    });
  }
}
