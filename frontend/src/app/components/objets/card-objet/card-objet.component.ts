import { Component, EventEmitter, Input, Output } from '@angular/core';
import { Objet } from "../../../models/objet";
import { MatButton } from "@angular/material/button";
import { MatCell } from "@angular/material/table";
import { CreerModifierObjetModalComponent } from "../creer-modifier-objet-modal/creer-modifier-objet-modal.component";
import { ApiService } from "../../../services/api.service";
import { MatDialog } from "@angular/material/dialog";

@Component({
  selector: 'app-card-objet',
  standalone: true,
  imports: [
    MatButton,
    MatCell
  ],
  templateUrl: './card-objet.component.html',
  styleUrl: './card-objet.component.scss'
})
export class CardObjetComponent {
  @Input({ required : true }) objet!: Objet;
  @Output() objetModifier = new EventEmitter<Objet>();
  @Output() objetSupprimer = new EventEmitter<number>();

  constructor(private api: ApiService, public dialog: MatDialog) {
  }

  modifierObjet() {
    const dialogRef = this.dialog.open(CreerModifierObjetModalComponent, {
      height: '400px',
      width: '800px',
      data: {action: 'modifier', objet: this.objet}
    });

    dialogRef.afterClosed().subscribe(objet => {
      if (objet) {
        this.objetModifier.emit(objet);
      }
    });
  }

  supprimerObjet() {
    this.objetSupprimer.emit(this.objet.id);
  }
}
