import { Component, OnInit } from '@angular/core';
import { ApiService } from "../../services/api.service";
import { Objet } from "../../models/objet";
import { ListeObjetComponent } from "./liste-objet/liste-objet.component";
import { MatDialog } from "@angular/material/dialog";
import { MatButton } from "@angular/material/button";
import { CreerModifierObjetModalComponent } from "./creer-modifier-objet-modal/creer-modifier-objet-modal.component";

@Component({
  selector: 'app-objets',
  standalone: true,
  imports: [
    ListeObjetComponent,
    MatButton
  ],
  templateUrl: './objets.component.html',
  styleUrl: './objets.component.scss'
})
export class ObjetsComponent implements OnInit {
  objets: Objet[] = [];

  constructor(private api: ApiService, public dialog: MatDialog) {
  }

  ngOnInit() {
    this.getObjets();
  }

  creerObjet() {
    const dialogRef = this.dialog.open(CreerModifierObjetModalComponent, {
      height: '400px',
      width: '800px',
      data: {action: 'creer', utilisateur: {}}
    });

    dialogRef.afterClosed().subscribe(objet => {
      if (objet) {
        this.api.creerObjet(objet).subscribe({
          next: () => {
            this.getObjets()
          }
        });
      }
    });
  }

  getObjets() {
    this.api.getObjets().subscribe((objets) => {
      this.objets = objets;
    });
  }
}
