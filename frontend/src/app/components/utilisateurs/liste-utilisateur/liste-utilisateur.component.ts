import { Component, Input, OnChanges, SimpleChanges } from '@angular/core';
import { Objet } from '../../../models/objet';
import { ApiService } from '../../../services/api.service';
import { MatDialog } from '@angular/material/dialog';
import { Utilisateur } from '../../../models/utilisateur';
import {
  CreerModifierUtilisateurModalComponent
} from '../creer-modifier-utilisateur-modal/creer-modifier-utilisateur-modal.component';
import { MatTableModule } from '@angular/material/table';
import { MatButton } from "@angular/material/button";
import { ListeObjetComponent } from "../../objets/liste-objet/liste-objet.component";

@Component({
  selector: 'app-liste-utilisateur',
  standalone: true,
  imports: [
    MatTableModule,
    MatButton,
    ListeObjetComponent
  ],
  templateUrl: './liste-utilisateur.component.html',
  styleUrl: './liste-utilisateur.component.scss'
})
export class ListeUtilisateurComponent {
  @Input() utilisateurs: Utilisateur[] = [];
  displayedColumns: string[] = ['nom', 'prenom', 'evaluation', 'adresse', 'actions'];
  utilisateurSelectionne!: Utilisateur;

  constructor(private api: ApiService, public dialog: MatDialog) {
  }

  modifierUtilisateur(utilisateur: Utilisateur) {
    const dialogRef = this.dialog.open(CreerModifierUtilisateurModalComponent, {
      height: '400px',
      width: '800px',
      data: {action: 'modifier', utilisateur: utilisateur}
    });

    dialogRef.afterClosed().subscribe(utilisateurModifier => {
      if (utilisateurModifier) {
        this.api.modifierUtilisateur(utilisateurModifier);
      }
    });
  }

  supprimerUtilisateur(id: number) {
    this.api.supprimerUtilisateur(id).subscribe( {
      next: () => {
        this.utilisateurs = this.utilisateurs.filter(utilisateur => utilisateur.id !== id);
      }
    });
  }

  recupererRow(utilisateur: Utilisateur) {
    console.log(utilisateur);
    this.utilisateurSelectionne = utilisateur;
  }

  envoyerObjetUtilisateur() {
    if (this.utilisateurSelectionne) {
      return this.utilisateurSelectionne.objets;
    }
    return []
  }
}
