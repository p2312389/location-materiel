import { Component, OnInit, Output } from '@angular/core';
import { ApiService } from '../../services/api.service';
import { Utilisateur } from '../../models/utilisateur';
import { ListeUtilisateurComponent } from './liste-utilisateur/liste-utilisateur.component';
import {
  CreerModifierUtilisateurModalComponent
} from "./creer-modifier-utilisateur-modal/creer-modifier-utilisateur-modal.component";
import { MatDialog } from "@angular/material/dialog";
import { MatButton } from "@angular/material/button";

@Component({
  selector: 'app-utilisateurs',
  standalone: true,
  imports: [ListeUtilisateurComponent, MatButton],
  templateUrl: './utilisateurs.component.html',
  styleUrl: './utilisateurs.component.scss'
})
export class UtilisateursComponent implements OnInit {
  utilisateurs: Utilisateur[] = [];

  constructor(private api: ApiService, public dialog: MatDialog) {
  }

  ngOnInit() {
    this.getUtilisateurs()
  }

  creerUtilisateur() {
    const dialogRef = this.dialog.open(CreerModifierUtilisateurModalComponent, {
      height: '400px',
      width: '800px',
      data: {action: 'creer', utilisateur: {}}
    });

    dialogRef.afterClosed().subscribe(utilisateur => {
      if (utilisateur) {
        this.api.creerUtilisateur(utilisateur).subscribe({
          next: () => {
            this.getUtilisateurs()
          }
        });
      }
    });
  }

  getUtilisateurs() {
    this.api.getUtilisateurs().subscribe((utilisateurs) => {
      this.utilisateurs = utilisateurs;
    });
  }
}
