import { Component, Inject } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogClose, MatDialogRef } from '@angular/material/dialog';
import { MatButton } from '@angular/material/button';
import { MatFormField, MatLabel } from '@angular/material/form-field';
import { FormControl, FormGroup, ReactiveFormsModule, Validators } from '@angular/forms';
import { MatOption, MatSelect } from '@angular/material/select';
import { MatInput } from '@angular/material/input';
import { Utilisateur, UtilisateurDto } from '../../../models/utilisateur';

interface dataModal {
  action: 'creer' | 'modifier';
  utilisateur?: Utilisateur;
}

@Component({
  selector: 'app-creer-modifier-utilisateur-modal',
  standalone: true,
  imports: [
    MatDialogClose,
    MatButton,
    MatFormField,
    ReactiveFormsModule,
    MatSelect,
    MatOption,
    MatInput,
    MatLabel
  ],
  templateUrl: './creer-modifier-utilisateur-modal.component.html',
  styleUrl: './creer-modifier-utilisateur-modal.component.scss'
})
export class CreerModifierUtilisateurModalComponent {
  utilisateurFormGroup: FormGroup = new FormGroup({
    prenom: new FormControl((this.data.utilisateur?.prenom || ''), Validators.required),
    nom: new FormControl((this.data.utilisateur?.nom || ''), Validators.required),
    evaluation: new FormControl((this.data.utilisateur?.evaluation || 0), Validators.required),
    adresse: new FormControl((this.data.utilisateur?.adresse || ''), Validators.required),
    ville: new FormControl((this.data.utilisateur?.ville || ''), Validators.required),
    codePostal: new FormControl((this.data.utilisateur?.codePostal || ''), Validators.required),
  });

  constructor(
    public dialogRef: MatDialogRef<CreerModifierUtilisateurModalComponent>,
    @Inject(MAT_DIALOG_DATA) public data: dataModal) {
  }

  onNoClick(): void {
    this.dialogRef.close();
  }

  creerUtilisateur() {
    let nouveauUtilisateur: UtilisateurDto = {
      nom: this.utilisateurFormGroup.controls['nom'].value,
      prenom: this.utilisateurFormGroup.controls['prenom'].value,
      evaluation: parseInt(this.utilisateurFormGroup.controls['evaluation'].value),
      adresse: this.utilisateurFormGroup.controls['adresse'].value,
      ville: this.utilisateurFormGroup.controls['ville'].value,
      codePostal: this.utilisateurFormGroup.controls['codePostal'].value
    }
    this.dialogRef.close(nouveauUtilisateur);
  }

  modifierUtilisateur() {
    if (this.data.utilisateur) {
      this.data.utilisateur.nom = this.utilisateurFormGroup.controls['nom'].value;
      this.data.utilisateur.prenom = this.utilisateurFormGroup.controls['prenom'].value;
      this.data.utilisateur.evaluation = parseInt(this.utilisateurFormGroup.controls['evaluation'].value);
      this.data.utilisateur.adresse = this.utilisateurFormGroup.controls['adresse'].value;
      this.data.utilisateur.ville = this.utilisateurFormGroup.controls['ville'].value;
      this.data.utilisateur.codePostal = this.utilisateurFormGroup.controls['codePostal'].value;

      this.dialogRef.close(this.data.utilisateur);
    }
  }
}
