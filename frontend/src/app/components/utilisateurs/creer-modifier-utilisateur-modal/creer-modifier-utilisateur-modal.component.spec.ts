import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CreerModifierUtilisateurModalComponent } from './creer-modifier-utilisateur-modal.component';

describe('CreerModifierUtilisateurModalComponent', () => {
  let component: CreerModifierUtilisateurModalComponent;
  let fixture: ComponentFixture<CreerModifierUtilisateurModalComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [CreerModifierUtilisateurModalComponent]
    })
    .compileComponents();
    
    fixture = TestBed.createComponent(CreerModifierUtilisateurModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
