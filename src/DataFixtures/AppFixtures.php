<?php

namespace App\DataFixtures;

use App\Entity\Objet;
use App\Entity\Transaction;
use App\Entity\Utilisateur;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;

class AppFixtures extends Fixture
{
    public function load(ObjectManager $manager): void
    {

        $utilisateurs = array();
        for ($i = 0; $i < 5; $i++) {
            $utilisateur = new Utilisateur();

            $utilisateur->setNom('Nom' . $i);
            $utilisateur->setPrenom('Prenom' . $i);
            $utilisateur->setAdresse('Adresse' . $i);
            $utilisateur->setVille('Ville' . $i);
            $utilisateur->setCodePostal('Code postal' . $i);
            $utilisateur->setEvaluation(mt_rand(0, 5));

            $utilisateurs[] = $utilisateur;

            $manager->persist($utilisateur);
        }

        $objets = array();
        for ($i = 0; $i < 20; $i++) {

            $objet = new Objet();

            $objet->setNom('Objet ' . $i);
            $objet->setDuree(mt_rand(0, 10));
            $objet->setPrix(mt_rand(10, 50));
            $objet->setProprietaire($utilisateurs[mt_rand(0, 4)]);

            $objets[] = $objet;

            $manager->persist($objet);

        }

        $manager->flush();


        for ($i = 0; $i < 20; $i++) {
            $transaction = new Transaction();

            $transaction->setClient($utilisateurs[mt_rand(0, 4)]);
            $transaction->setLoueur($utilisateurs[mt_rand(0, 4)]);
            $transaction->setObjet($objets[mt_rand(0, 19)]);
            $transaction->setTemps(mt_rand(1, 20));
            $transaction->setDateDebut(new \DateTime());
            $transaction->setDateFin(new \DateTime());

            $manager->persist($transaction);
        }

        $manager->flush();
    }
}
