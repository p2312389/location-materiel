<?php

namespace App\Entity;

use ApiPlatform\Metadata\ApiResource;
use App\Repository\UtilisateurRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

#[ORM\Entity(repositoryClass: UtilisateurRepository::class)]
#[ApiResource(normalizationContext: ['groups' => ['utilisateur']])]
class Utilisateur
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    #[Groups(['objet', 'utilisateur', 'transaction'])]
    private ?int $id = null;

    #[ORM\Column(length: 255)]
    #[Groups(['objet', 'utilisateur', 'transaction'])]
    private ?string $nom = null;

    #[ORM\Column(length: 255)]
    #[Groups(['objet', 'utilisateur', 'transaction'])]
    private ?string $prenom = null;

    #[ORM\Column(nullable: true)]
    #[Groups(['utilisateur'])]
    private ?int $evaluation = null;

    #[ORM\Column(length: 255, nullable: true)]
    #[Groups(['utilisateur'])]
    private ?string $adresse = null;

    #[ORM\Column(length: 255, nullable: true)]
    #[Groups(['utilisateur'])]
    private ?string $ville = null;

    #[ORM\Column(length: 255, nullable: true)]
    #[Groups(['utilisateur'])]
    private ?string $codePostal = null;

    /**
     * @var Collection<int, Transaction>
     */
    #[ORM\OneToMany(targetEntity: Transaction::class, mappedBy: 'loueur')]
    private Collection $transactionsLoueur;

    /**
     * @var Collection<int, Transaction>
     */
    #[ORM\OneToMany(targetEntity: Transaction::class, mappedBy: 'client')]
    private Collection $transactionsClient;

    /**
     * @var Collection<int, Objet>
     */
    #[ORM\OneToMany(targetEntity: Objet::class, mappedBy: 'proprietaire')]
    #[Groups(['utilisateur'])]
    private Collection $objets;

    public function __construct()
    {
        $this->transactionsLoueur = new ArrayCollection();
        $this->transactionsClient = new ArrayCollection();
        $this->objets = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNom(): ?string
    {
        return $this->nom;
    }

    public function setNom(string $nom): static
    {
        $this->nom = $nom;

        return $this;
    }

    public function getPrenom(): ?string
    {
        return $this->prenom;
    }

    public function setPrenom(string $prenom): static
    {
        $this->prenom = $prenom;

        return $this;
    }

    public function getEvaluation(): ?int
    {
        return $this->evaluation;
    }

    public function setEvaluation(?int $evaluation): static
    {
        $this->evaluation = $evaluation;

        return $this;
    }

    public function getAdresse(): ?string
    {
        return $this->adresse;
    }

    public function setAdresse(?string $adresse): static
    {
        $this->adresse = $adresse;

        return $this;
    }

    public function getVille(): ?string
    {
        return $this->ville;
    }

    public function setVille(?string $ville): static
    {
        $this->ville = $ville;

        return $this;
    }

    public function getCodePostal(): ?string
    {
        return $this->codePostal;
    }

    public function setCodePostal(?string $codePostal): static
    {
        $this->codePostal = $codePostal;

        return $this;
    }

    /**
     * @return Collection<int, Transaction>
     */
    public function getTransactionsLoueur(): Collection
    {
        return $this->transactionsLoueur;
    }

    public function addTransactionsLoueur(Transaction $transactionsLoueur): static
    {
        if (!$this->transactionsLoueur->contains($transactionsLoueur)) {
            $this->transactionsLoueur->add($transactionsLoueur);
            $transactionsLoueur->setLoueur($this);
        }

        return $this;
    }

    public function removeTransactionsLoueur(Transaction $transactionsLoueur): static
    {
        if ($this->transactionsLoueur->removeElement($transactionsLoueur)) {
            // set the owning side to null (unless already changed)
            if ($transactionsLoueur->getLoueur() === $this) {
                $transactionsLoueur->setLoueur(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection<int, Transaction>
     */
    public function getTransactionsClient(): Collection
    {
        return $this->transactionsClient;
    }

    public function addTransactionsClient(Transaction $transactionsClient): static
    {
        if (!$this->transactionsClient->contains($transactionsClient)) {
            $this->transactionsClient->add($transactionsClient);
            $transactionsClient->setClient($this);
        }

        return $this;
    }

    public function removeTransactionsClient(Transaction $transactionsClient): static
    {
        if ($this->transactionsClient->removeElement($transactionsClient)) {
            // set the owning side to null (unless already changed)
            if ($transactionsClient->getClient() === $this) {
                $transactionsClient->setClient(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection<int, Objet>
     */
    public function getObjets(): Collection
    {
        return $this->objets;
    }

    public function addObjet(Objet $objet): static
    {
        if (!$this->objets->contains($objet)) {
            $this->objets->add($objet);
            $objet->setProprietaire($this);
        }

        return $this;
    }

    public function removeObjet(Objet $objet): static
    {
        if ($this->objets->removeElement($objet)) {
            // set the owning side to null (unless already changed)
            if ($objet->getProprietaire() === $this) {
                $objet->setProprietaire(null);
            }
        }

        return $this;
    }
}
