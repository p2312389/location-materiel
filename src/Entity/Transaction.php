<?php

namespace App\Entity;

use ApiPlatform\Metadata\ApiResource;
use App\Repository\TransactionRepository;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

#[ORM\Entity(repositoryClass: TransactionRepository::class)]
#[ApiResource(normalizationContext: ['groups' => ['transaction']])]
class Transaction
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    #[Groups(['objet', 'transaction'])]
    private ?int $id = null;

    #[ORM\Column(type: Types::DATE_MUTABLE, nullable: true)]
    #[Groups(['objet', 'transaction'])]
    private ?\DateTimeInterface $dateDebut = null;

    #[ORM\Column(type: Types::DATE_MUTABLE, nullable: true)]
    #[Groups(['objet', 'transaction'])]
    private ?\DateTimeInterface $dateFin = null;

    #[ORM\Column(nullable: true)]
    #[Groups(['objet', 'transaction'])]
    private ?int $temps = null;

    #[ORM\ManyToOne(inversedBy: 'transactionsLoueur')]
    #[ORM\JoinColumn(onDelete:"CASCADE")]
    #[Groups(['transaction'])]
    private ?Utilisateur $loueur = null;

    #[ORM\ManyToOne(inversedBy: 'transactionsClient')]
    #[ORM\JoinColumn(onDelete:"CASCADE")]
    #[Groups(['transaction'])]
    private ?Utilisateur $client = null;

    #[ORM\ManyToOne(inversedBy: 'transactions')]
    #[ORM\JoinColumn(onDelete:"CASCADE")]
    #[Groups(['transaction'])]
    private ?Objet $objet = null;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getDateDebut(): ?\DateTimeInterface
    {
        return $this->dateDebut;
    }

    public function setDateDebut(?\DateTimeInterface $dateDebut): static
    {
        $this->dateDebut = $dateDebut;

        return $this;
    }

    public function getDateFin(): ?\DateTimeInterface
    {
        return $this->dateFin;
    }

    public function setDateFin(?\DateTimeInterface $dateFin): static
    {
        $this->dateFin = $dateFin;

        return $this;
    }

    public function getTemps(): ?int
    {
        return $this->temps;
    }

    public function setTemps(?int $temps): static
    {
        $this->temps = $temps;

        return $this;
    }

    public function getLoueur(): ?Utilisateur
    {
        return $this->loueur;
    }

    public function setLoueur(?Utilisateur $loueur): static
    {
        $this->loueur = $loueur;

        return $this;
    }

    public function getClient(): ?Utilisateur
    {
        return $this->client;
    }

    public function setClient(?Utilisateur $client): static
    {
        $this->client = $client;

        return $this;
    }

    public function getObjet(): ?Objet
    {
        return $this->objet;
    }

    public function setObjet(?Objet $objet): static
    {
        $this->objet = $objet;

        return $this;
    }
}
