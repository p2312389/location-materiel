<?php

namespace App\Entity;

use ApiPlatform\Metadata\ApiResource;
use App\Repository\ObjetRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

#[ORM\Entity(repositoryClass: ObjetRepository::class)]
#[ApiResource(normalizationContext: ['groups' => ['objet']])]
class Objet
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    #[Groups(['objet', 'utilisateur', 'transaction'])]
    private ?int $id = null;

    #[ORM\Column(length: 255)]
    #[Groups(['objet', 'utilisateur', 'transaction'])]
    private ?string $nom = null;

    #[ORM\Column(nullable: true)]
    #[Groups(['objet', 'utilisateur', 'transaction'])]
    private ?float $prix = null;

    #[ORM\Column(nullable: true)]
    #[Groups(['objet', 'utilisateur', 'transaction'])]
    private ?int $duree = null;

    /**
     * @var Collection<int, Transaction>
     */
    #[ORM\OneToMany(targetEntity: Transaction::class, mappedBy: 'objet')]
    #[ORM\JoinColumn(onDelete:"CASCADE")]
    #[Groups('objet')]
    private Collection $transactions;

    #[ORM\ManyToOne(inversedBy: 'objets')]
    #[ORM\JoinColumn(onDelete:"CASCADE")]
    #[Groups('objet')]
    private ?Utilisateur $proprietaire = null;

    public function __construct()
    {
        $this->transactions = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNom(): ?string
    {
        return $this->nom;
    }

    public function setNom(string $nom): static
    {
        $this->nom = $nom;

        return $this;
    }

    public function getPrix(): ?float
    {
        return $this->prix;
    }

    public function setPrix(?float $prix): static
    {
        $this->prix = $prix;

        return $this;
    }

    public function getDuree(): ?int
    {
        return $this->duree;
    }

    public function setDuree(?int $duree): static
    {
        $this->duree = $duree;

        return $this;
    }

    /**
     * @return Collection<int, Transaction>
     */
    public function getTransactions(): Collection
    {
        return $this->transactions;
    }

    public function addTransaction(Transaction $transaction): static
    {
        if (!$this->transactions->contains($transaction)) {
            $this->transactions->add($transaction);
            $transaction->setObjet($this);
        }

        return $this;
    }

    public function removeTransaction(Transaction $transaction): static
    {
        if ($this->transactions->removeElement($transaction)) {
            // set the owning side to null (unless already changed)
            if ($transaction->getObjet() === $this) {
                $transaction->setObjet(null);
            }
        }

        return $this;
    }

    public function getProprietaire(): ?Utilisateur
    {
        return $this->proprietaire;
    }

    public function setProprietaire(?Utilisateur $proprietaire): static
    {
        $this->proprietaire = $proprietaire;

        return $this;
    }
}
