<?php

namespace App\Controller;

use App\Entity\Objet;
use App\Repository\UtilisateurRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Attribute\Route;
use Symfony\Component\Serializer\Normalizer\AbstractNormalizer;
use Symfony\Component\Serializer\SerializerInterface;

#[Route('api')]
class ObjetController extends AbstractController
{
    private SerializerInterface $serializer;
    private UtilisateurRepository $utilisateurRepository;
    private EntityManagerInterface $entityManager;

    public function __construct(
        SerializerInterface $serializer, UtilisateurRepository $utilisateurRepository, EntityManagerInterface $entityManager)
    {
        $this->serializer = $serializer;
        $this->utilisateurRepository = $utilisateurRepository;
        $this->entityManager = $entityManager;
    }

    #[Route('/objet/{id}', name: 'modifier-objet', methods: 'PUT')]
    public function modifierObjet(Request $request, Objet $objet)
    {
        $updatedObjet = $this->serializer->deserialize($request->getContent(),
            Objet::class,
            'json',
            [AbstractNormalizer::OBJECT_TO_POPULATE => $objet]);
        $content = $request->toArray();
        $idProprietaire = $content['idProprietaire'];
        $updatedObjet->setProprietaire($this->utilisateurRepository->find($idProprietaire));

        $this->entityManager->persist($updatedObjet);
        $this->entityManager->flush();
        return new JsonResponse(null, Response::HTTP_NO_CONTENT);
    }

    #[Route('/objet/{id}', name: 'supprimer-objet', methods: 'DELETE')]
    public function supprimerObjet(Objet $objet)
    {
        $this->entityManager->remove($objet);
        $this->entityManager->flush();

        return new JsonResponse(null, Response::HTTP_NO_CONTENT);
    }

    #[Route('/objet', name: 'creer-objet', methods: 'POST')]
    public function creerObjet(Request $request)
    {
        $objet = $this->serializer->deserialize($request->getContent(), Objet::class, 'json');

        // Récupération de l'ensemble des données envoyées sous forme de tableau
        $content = $request->toArray();

        if (!array_key_exists('idProprietaire', $content)) {

            return new JsonResponse("Veuillez renseigner tous les champs nécessaires", Response::HTTP_NOT_FOUND, [], true);
        }

        $idProprietaire = $content['idProprietaire'];

        $objet->setProprietaire($this->utilisateurRepository->find($idProprietaire));

        $this->entityManager->persist($objet);
        $this->entityManager->flush();

        $jsonBook = $this->serializer->serialize($objet, 'json', ['groups' => 'objet']);

        return new JsonResponse($jsonBook, Response::HTTP_CREATED, [], true);
    }
}
