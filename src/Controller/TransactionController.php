<?php

namespace App\Controller;

use App\Entity\Transaction;
use App\Repository\ObjetRepository;
use App\Repository\UtilisateurRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Attribute\Route;
use Symfony\Component\Serializer\Normalizer\AbstractNormalizer;
use Symfony\Component\Serializer\SerializerInterface;

#[Route('api')]
class TransactionController extends AbstractController
{
    private SerializerInterface $serializer;
    private UtilisateurRepository $utilisateurRepository;
    private ObjetRepository $objetRepository;
    private EntityManagerInterface $entityManager;

    public function __construct(
        SerializerInterface $serializer, UtilisateurRepository $utilisateurRepository, ObjetRepository $objetRepository, EntityManagerInterface $entityManager)
    {
        $this->serializer = $serializer;
        $this->utilisateurRepository = $utilisateurRepository;
        $this->objetRepository = $objetRepository;
        $this->entityManager = $entityManager;
    }

    #[Route('/transaction/{id}', name: 'modifier-transaction', methods: 'PUT')]
    public function modifierTransaction(Request $request, Transaction $transaction)
    {
        $updatedTransaction = $this->serializer->deserialize($request->getContent(),
            Transaction::class,
            'json',
            [AbstractNormalizer::OBJECT_TO_POPULATE => $transaction]);
        $content = $request->toArray();

        if (array_key_exists('idLoueur', $content)) {
            $idLoueur = $content['idLoueur'];
            $updatedTransaction->setLoueur($this->utilisateurRepository->find($idLoueur));
        }
        if (array_key_exists('idClient', $content)) {
            $idClient = $content['idClient'];
            $updatedTransaction->setClient($this->utilisateurRepository->find($idClient));
        }
        if (array_key_exists('idObjet', $content)) {
            $idObjet = $content['idObjet'];
            $updatedTransaction->setObjet($this->utilisateurRepository->find($idObjet));
        }

        $this->entityManager->persist($updatedTransaction);
        $this->entityManager->flush();
        return new JsonResponse(null, Response::HTTP_NO_CONTENT);
    }

    #[Route('/transaction/{id}', name: 'supprimer-transaction', methods: 'DELETE')]
    public function supprimerTransaction(Transaction $transaction)
    {
        $this->entityManager->remove($transaction);
        $this->entityManager->flush();

        return new JsonResponse(null, Response::HTTP_NO_CONTENT);
    }

    #[Route('/transaction', name: 'creer-transaction', methods: 'POST')]
    public function creerTransaction(Request $request)
    {
        $transaction = $this->serializer->deserialize($request->getContent(), Transaction::class, 'json');

        // Récupération de l'ensemble des données envoyées sous forme de tableau
        $content = $request->toArray();

        if (!array_key_exists('idLoueur', $content) && !array_key_exists('idClient', $content) && !array_key_exists('idObjet', $content)) {

            return new JsonResponse("Veuillez renseigner tous les champs nécessaires", Response::HTTP_NOT_FOUND, [], true);
        }

        $idLoueur = $content['idLoueur'];
        $idClient = $content['idClient'];
        $idObjet = $content['idObjet'];

        $transaction->setLoueur($this->utilisateurRepository->find($idLoueur));
        $transaction->setClient($this->utilisateurRepository->find($idClient));
        $transaction->setObjet($this->utilisateurRepository->find($idObjet));

        $this->entityManager->persist($transaction);
        $this->entityManager->flush();

        $jsonTransaction = $this->serializer->serialize($transaction, 'json', ['groups' => 'transaction']);

        return new JsonResponse($jsonTransaction, Response::HTTP_CREATED, [], true);
    }
}
