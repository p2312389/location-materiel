<?php

namespace App\Controller;

use App\Entity\Objet;
use App\Entity\Utilisateur;
use App\Repository\UtilisateurRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Attribute\Route;
use Symfony\Component\Serializer\Normalizer\AbstractNormalizer;
use Symfony\Component\Serializer\SerializerInterface;

#[Route('api')]
class UtilisateurController extends AbstractController
{
    private SerializerInterface $serializer;
    private EntityManagerInterface $entityManager;

    public function __construct(
        SerializerInterface $serializer, EntityManagerInterface $entityManager)
    {
        $this->serializer = $serializer;
        $this->entityManager = $entityManager;
    }

    #[Route('/utilisateur/{id}', name: 'modifier-utilisateur', methods: 'PUT')]
    public function modifierUtilisateur(Request $request, Utilisateur $utilisateur)
    {
        $updatedUtilisateur = $this->serializer->deserialize($request->getContent(),
            Utilisateur::class,
            'json',
            [AbstractNormalizer::OBJECT_TO_POPULATE => $utilisateur]);

        $this->entityManager->persist($updatedUtilisateur);
        $this->entityManager->flush();
        return new JsonResponse(null, Response::HTTP_NO_CONTENT);
    }

    #[Route('/utilisateur/{id}', name: 'supprimer-utilisateur', methods: 'DELETE')]
    public function supprimerUtilisateur(Utilisateur $utilisateur)
    {
        $this->entityManager->remove($utilisateur);
        $this->entityManager->flush();

        return new JsonResponse(null, Response::HTTP_NO_CONTENT);
    }

    #[Route('/utilisateur', name: 'creer-utilisateur', methods: 'POST')]
    public function creerUtilisateur(Request $request)
    {
        $utilisateur = $this->serializer->deserialize($request->getContent(), Utilisateur::class, 'json');

        $this->entityManager->persist($utilisateur);
        $this->entityManager->flush();

        $jsonUser = $this->serializer->serialize($utilisateur, 'json', ['groups' => 'utilisateur']);

        return new JsonResponse($jsonUser, Response::HTTP_CREATED, [], true);
    }
}
